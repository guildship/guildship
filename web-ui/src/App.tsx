import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "ui/screens/Home";
import About from "ui/screens/About";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/about" component={About} />
      </Switch>
    </Router>
  );
}

export default App;
