import React from "react";
import styled from "@emotion/styled/macro";

const Wrapper = styled.div`
  display: inline-block;
  width: ${p => p.size}px;
  height: ${p => p.size}px;
  border-radius: ${p => p.size * 4}px;
  box-shadow: inset 0 0 0 1px rgba(255, 255, 255, 0.5),
    0 0 0 1px rgba(0, 0, 0, 0.5);
  background-image: url(${p => p.url});
  background-size: cover;
`;

export default function Avatar(props: {
  description: string;
  size: number;
  url: string;
}) {
  return (
    <Wrapper
      role="presentation"
      aria-label={props.description}
      title={props.description}
      size={props.size || 96}
      url={props.url}
      backgroundImage={props.url}
    />
  );
}
