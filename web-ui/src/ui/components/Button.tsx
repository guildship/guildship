import React from "react";
import css from "@emotion/css/macro";
import styled from "@emotion/styled/macro";
import chroma from "chroma-js";

const desiredContrast = 2.3;

function getContrastingColor(color, lightColor, darkColor) {
  if (chroma.contrast(color, "white") >= desiredContrast) {
    return lightColor;
  }
  if (chroma.contrast(color, darkColor) > desiredContrast) {
    return darkColor;
  }
}

function getBackgroundColor({ color, theme }) {
  if (color === "normal") {
    return theme.colors.GSGray[300];
  }
  if (theme.colors[color] === undefined) {
    return color;
  }
  return theme.colors[color][500];
}

function getBackgroundGradient(props) {
  const { shiny } = props;
  if (!shiny) {
    return "";
  }
  const bg = getBackgroundColor(props);

  return `linear-gradient(
    135deg,
    ${chroma(bg).brighten(2)},
    ${chroma(bg).alpha(0)} 66%)`;
}

function getColor(p) {
  const backgroundColor = getBackgroundColor(p);

  return getContrastingColor(
    backgroundColor,
    p.theme.colors.text,
    p.theme.colors.base,
  );
}

function getTextShadowColor(p) {
  const color = getColor(p);
  return getContrastingColor(color, "white", "black");
}

function getGlow(props) {
  if (!props.glow) return "";
  return css`
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    z-index: -1 !important;
    filter: blur(16px) brightness(1.5) saturate(1.5) opacity(0.5);
    background: inherit;
  `;
}

function getBorderColor({ color, theme }) {
  const base =
    theme.colors[color] !== undefined ? theme.colors[color][500] : color;
  return chroma
    .mix(base, "black", 0.75, "lab")
    .alpha(0.25)
    .toString();
}

const StyledButton = styled.button`
  position: relative;
  border: none;
  cursor: pointer;
  padding: 0.5em 1.5em;
  border-radius: 3px;
  display: inline-block;
  color: ${getColor};
  text-shadow: 0 1px 1px
    ${p =>
      chroma(getTextShadowColor(p))
        .alpha(0.666)
        .toString()};
  background-color: ${getBackgroundColor};
  background-image: ${getBackgroundGradient};
  transition: all 0.1s ease;
  box-shadow: inset 0 0 0 1px ${getBorderColor};

  &::selection {
    background: transparent;
  }

  &:hover,
  &:focus {
    z-index: 1;
    background-color: ${p =>
      chroma(getBackgroundColor(p))
        .brighten(0.25)
        .toString()};
  }

  &:active {
    background-color: ${p =>
      chroma(getBackgroundColor(p))
        .brighten(1)
        .toString()};
  }

  &::after {
    ${getGlow};
    transition: background 0.2s ease, transform 0.2s ease;
  }
`;

export default function Button(props) {
  return (
    <StyledButton
      {...props}
      color={props.color || props.rarity || props.type}
      glow={props.rarity || props.glow}
      shiny={props.rarity || props.shiny}
    />
  );
}

Button.defaultProps = {
  type: "normal",
};
