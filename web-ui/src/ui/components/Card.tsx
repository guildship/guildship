import React from "react";
import styled from "@emotion/styled/macro";
import { Card as _Card } from "@rebass/emotion";

const Wrapper = styled(_Card)`
  > *:first-child,
  > *:last-child {
    margin-left: inherit;
    margin-right: inherit;
    margin-top: 0;
    margin-bottom: 0;
  }
`;

function Card(props) {
  return (
    <Wrapper
      bg="#fafafa"
      border="1px solid #f0f0f0"
      borderRadius={3}
      p="1rem"
      height="100%"
      width="100%"
      {...props}
    />
  );
}

export default Card;
