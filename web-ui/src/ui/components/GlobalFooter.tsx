import React from "react";
import styled from "@emotion/styled/macro";
import chroma from "chroma-js";
import { themeGet } from "styled-system";

const Footer = styled.footer`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 2.5rem;
  text-align: center;
  background-color: ${themeGet("colors.base")};
`;

const RedColor = styled.span`
  color: ${themeGet("colors.GSRed.600")};
  text-shadow: 0 0 16px ${themeGet("colors.GSRed.600")};
`;

const Text = styled.span`
  color: ${p => p.theme.colors.text.toString()};
  text-shadow: 0 0 16px
    ${p =>
      chroma(p.theme.colors.text)
        .alpha(0.75)
        .toString()};
`;

export default function GlobalFooter() {
  return (
    <Footer>
      <Text>
        Made with <RedColor>&hearts;</RedColor> in Louisville, KY.
      </Text>
    </Footer>
  );
}
