import { createContext } from "react";

import { theme } from "ui/styles/themes";

export default createContext(theme);
