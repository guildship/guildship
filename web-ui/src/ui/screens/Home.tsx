import React from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";

function Home() {
  return (
    <>
      <Helmet>
        <title>Guildship</title>
      </Helmet>
      <div>
        <h1>Guildship</h1>
        <div>
          <Link to="/about">About</Link>
        </div>
      </div>
    </>
  );
}

export default Home;
