import chroma from "chroma-js";

const genPalette = (...colors) => {
  return chroma
    .scale(colors)
    .mode("lch")
    .colors(11)
    .reduce(
      (acc, curr, i) => ({
        ...acc,
        [i.toString() + "00"]: curr,
      }),
      {},
    );
};

export const palette = {
  GSGray: genPalette(0x21262b, 0x7d8a99, 0xf8f9fa),
  GSRed: genPalette(0x4d0300, 0xff0900, 0xffebea),
  GSViolet: genPalette(0x29004d, 0x8800ff, 0xf5e9ff),
  GSGreen: genPalette(0x004d03, 0x00e708, 0xebffeb),
  GSBlue: genPalette(0x00244d, 0x0077ff, 0xe4f0ff),
  GSOrange: genPalette(0x4d1f00, 0xff6600, 0xffeee3),
  GSRose: genPalette(0x4d0024, 0xff0077, 0xffeaf4),
  GSYellow: genPalette(0x4d4500, 0xffe500, 0xfffbe0),
  GSIndigo: genPalette(0x01004d, 0x0800ff, 0xebebff),
  GSFoam: genPalette(0x004d42, 0x00ffdd, 0xe5fffc),
};

const rarityColors = {
  common: palette.GSGray,
  rare: palette.GSBlue,
  unique: palette.GSGreen,
  epic: palette.GSViolet,
  exotic: palette.GSOrange,
  legendary: palette.GSYellow,
};

const descriptivePalette = {
  normal: palette.GSGray,
  primary: palette.GSBlue,
  danger: palette.GSRed,
  caution: palette.GSOrange,
  success: palette.GSGreen,
};

const typography = {
  fontSizes: [12, 14, 16, 24, 32, 48, 64, 96, 128],
  fonts: {
    base: `-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"`,
  },
};

export const theme = {
  type: "dark",
  typography,
  colors: {
    base: palette.GSGray["000"],
    text: palette.GSGray[1000],
    ...palette,
    ...descriptivePalette,
    ...rarityColors,
    cardBackground: palette.GSGray[100],
  },
};
