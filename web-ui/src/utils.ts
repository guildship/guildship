import chroma from "chroma-js";

export const isLightColor = c => chroma(c).luminance() >= 0.45;
export const isDarkColor = c => !isLightColor(c);
